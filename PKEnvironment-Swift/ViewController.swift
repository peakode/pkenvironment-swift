//
//  ViewController.swift
//  PKEnvironment-Swift
//
//  Created by Gokhan Gultekin on 28/06/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var envLabel: UILabel?
    
    public func pkprint(_ items: Any..., separator: String = " ", terminator: String = "\n") {
        let output = items.map { "\n\nMeta:\nEnvironment: \(Config.environment())\n\nLog:\n\($0)" }.joined(separator: separator)
        Swift.print(output, terminator: terminator)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        var text = ""
        
        /*
         Gokhan:
         You can use switch-case structure below. It allows you to manage environments easily.
         */
       
        switch Config.environment() {
        case .Development:
            text = "DEVELOPMENT";
            break
        case .Test:
            text = "TEST";
            break
        case .AppStore:
            text = "APP STORE";
            break
        }
        
        pkprint("Logs won't be shown when enviroment is App Store!")

        envLabel?.text = text;
        
    }

}

