//
//  Config.swift
//  PKEnvironment-Swift
//
//  Created by Gokhan Gultekin on 28/06/2017.
//  Copyright © 2017 Peakode. All rights reserved.
//

import UIKit

enum Environment {
    case Development
    case Test
    case AppStore
}

class Config: NSObject {
    
    class func environment() -> Environment {
       
        var env: Environment = .Development
        
        #if APPSTORE
        env = .AppStore
        #endif

        #if DEVELOPMENT
        env = .Development
        #endif

        #if TEST
        env = .Test
        #endif
        
        return env
        
    }

    class func baseUrl() -> String {
       return Bundle.main.object(forInfoDictionaryKey: "PKBaseUrl") as! String
    }

}
